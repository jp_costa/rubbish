pub struct TrashFile {
    old_path: std::path::PathBuf,
    new_path: Option<std::path::PathBuf>,
    name: String,
}

impl TrashFile {
    pub fn new(args: Vec<String>) -> Result<TrashFile, &'static str> {
        if args.len() < 2 {
            return Err("Missing arguments");
        }
        let old_path: std::path::PathBuf;
        match get_file_path(&args[1]) {
            Ok(path) => old_path = path,
            Err(_) => return Err("File not found"),
        }
        let file_name: String = get_file_name(&old_path);
        Ok(TrashFile {
            old_path: old_path,
            name: file_name,
            new_path: None,
        })
    }

    pub fn trash_it(&mut self) -> Result<(), &'static str> {
        match self.move_to_trash_and_create_info_file() {
            Ok(()) => Ok(()),
            Err(_) => return Err("couldn't trash the file"),
        }
    }

    fn move_to_trash_and_create_info_file(&mut self) -> Result<(), std::io::Error> {
        let trash_can = TrashCan::new();
        //move
        let new_path = format!("{}{}", trash_can.files_path, self.name);
        std::fs::rename(&self.old_path, &new_path)?;
        //create trash_info file
        let file_metadata = std::fs::metadata(new_path)?;
        let deletion_date = file_metadata.modified()?;
        let trash_file_content = format!(
            "\
[Trash Info]\n \
Path={:?}\n \
DeletionDate={:?}",
            self.old_path, deletion_date
        );
        std::fs::write(
            format!("{}{}.trashinfo", trash_can.info_path, self.name),
            trash_file_content,
        )?;
        Ok(())
    }
}
struct TrashCan {
    files_path: &'static str,
    info_path: &'static str,
}

impl TrashCan {
    pub fn new() -> TrashCan {
        let files_path: &'static str = "trash/files/";
        let info_path: &'static str = "trash/info/";
        TrashCan {
            files_path,
            info_path,
        }
    }
}

fn get_file_path(given_path: &str) -> Result<std::path::PathBuf, std::io::Error> {
    let path = std::fs::canonicalize(given_path)?;
    Ok(path)
}

fn get_file_name(path: &std::path::PathBuf) -> String {
    let file_name = String::from(
        path.file_name()
            .expect("invalid file path")
            .to_str()
            .unwrap(),
    );
    file_name
}
