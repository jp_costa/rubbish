use rubbish::TrashFile;
fn main() {
    let args: Vec<String> = std::env::args().collect();
    let mut trash_file = TrashFile::new(args).unwrap_or_else(|err| {
        eprintln!("{:?}", err);
        std::process::exit(1);
    });
    trash_file.trash_it().unwrap_or_else(|err| {
        eprintln!("{:?}", err);
        std::process::exit(1);
    });
}
